﻿# 1st create the root certificate then the clien certificate
#-------------------------------------------------------

# You can change the subject names

# 1st select section of script and run

#----------------------------------------------------------

# Create a ROOT certificate from WINDOWS 10

$cert = New-SelfSignedCertificate -Type Custom -KeySpec Signature `
-Subject "CN=REBELROOT" -KeyExportPolicy Exportable `
-HashAlgorithm sha256 -KeyLength 2048 `
-CertStoreLocation "Cert:\CurrentUser\My" -KeyUsageProperty Sign -KeyUsage CertSign

# Create a client certificate from WINDOWS 10

New-SelfSignedCertificate -Type Custom -DnsName REBELCLIENT -KeySpec Signature `
-Subject "CN=REBELCLIENT" -KeyExportPolicy Exportable `
-HashAlgorithm sha256 -KeyLength 2048 `
-CertStoreLocation "Cert:\CurrentUser\My" `
-Signer $cert -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2")